import React, { Component } from "react";
import _ from "lodash";
import WeatherDetails from "./WeatherDetails";
import { WEATHER_API_KEY, MAP_API_KEY } from "./Constants";

class WeatherApp extends Component {
  constructor(props) {
    super(props);
    this.state = {
      searchText: "",
      lat: 51.75,
      lon: -3.38,
      /// These will be updated with data from the API, so are subject to change (point 2)
      location: "",
      weather: "",
      icon: ""
    };

    /// Google Map Vars
    this.map = null;
    this.marker = null;
    this.mapZoomLevel = 10;
  }

  componentDidMount() {
    const googleScript = document.createElement("script");
    googleScript.src = `https://maps.googleapis.com/maps/api/js?key=${MAP_API_KEY}`;
    window.document.body.appendChild(googleScript);

    googleScript.addEventListener("load", () => {
      /// Render a new map
      this.renderMap();

      /// Run update state, passing in the setup
      this.updateState(null, this.state.lat, this.state.lon);
    });
  }

  capitalizeFirstLetter = string => {
    return string.charAt(0).toUpperCase() + string.slice(1);
  };

  getData = (location, lat, lon) => {
    /// Variable to return
    let data;

    /// If it's a search
    if (location !== null) {
      data = fetch(
        "https://api.openweathermap.org/data/2.5/weather?q=" +
          location +
          "&APPID=" +
          WEATHER_API_KEY
      );
    } /// It's a pin drop
    else {
      data = fetch(
        "https://api.openweathermap.org/data/2.5/weather?lat=" +
          lat +
          "&lon=" +
          lon +
          "&APPID=" +
          WEATHER_API_KEY
      );
    }

    return data;
  };

  updateState = (locationName, lat, lon) => {
    /// Get data from the API, then set state with it
    this.getData(locationName, lat, lon).then(data => {
      /// Update the state, pass updateMap as a callback
      this.setState(
        {
          lat: _.get(data, "coord.lat"),
          lon: _.get(data, "coord.lon"),
          weather: this.capitalizeFirstLetter(
            _.get(data, "weather[0].description", "")
          ),
          location: data.name,
          icon:
            "https://openweathermap.org/img/w/" +
            _.get(data, "weather[0].icon", "") +
            ".png" /// Messy
        },
        this.updateMap
      ); /// Pass updateMap as a callback
    });
  };

  locationSearch = () => {
    /// Get the value from the search field
    const location = this.state.searchText;

    if (location !== "") {
      /// Update state with new API Data based on location name
      this.updateState(location, null, null);
    }
  };

  geolocationSearch = () => {
    /// Successful geolocation
    const success = position => {
      const lat = position.coords.latitude;
      const lon = position.coords.longitude;

      /// Update state with new API Data based on lat lon
      this.updateState(null, lat, lon);
    };

    /// Error'd geolocation
    var error = error => {
      if (error.message == "User denied Geolocation") {
        alert("Please enable location services");
      }
    };

    /// Get the position
    navigator.geolocation.getCurrentPosition(success, error);
  };

  formSubmit = e => {
    e.preventDefault();

    /// Clear the input
    this.setState({ searchText: "" });
  };

  renderMap = (lat, lng) => {
    /**
     * Map coordinates and pin coordinates are added in updateMap(),
     * which is run by updateStateWithData()
     */

    /// Create a new map
    this.map = new window.google.maps.Map(document.getElementById("map"), {
      zoom: this.mapZoomLevel,
      disableDefaultUI: true,
      zoomControl: true
    });

    /// Create a new marker
    this.marker = new window.google.maps.Marker({
      map: this.map,
      draggable: true
    });

    /// Set the initial pin drop animation
    this.marker.setAnimation(window.google.maps.Animation.DROP);

    /// Add an event listener for click
    window.google.maps.event.addListener(this.map, "click", event => {
      const latLng = event.latLng;
      const lat = latLng.lat();
      const lng = latLng.lng();

      /// Update state based on lat lon
      this.updateState(null, lat, lng);
    });

    /// Add an event listener for drag end
    window.google.maps.event.addListener(this.marker, "dragend", event => {
      const latLng = event.latLng;
      const lat = latLng.lat();
      const lng = latLng.lng();
      /// Update state based on lat lon
      this.updateState(null, lat, lng);
    });

    /// Update variable on map change
    this.map.addListener("zoom_changed", () => {
      this.mapZoomLevel = this.map.getZoom();
    });
  };

  updateMap = (lat, lon) => {
    var latLng = new window.google.maps.LatLng(this.state.lat, this.state.lon);

    /// Set a timeout before doing map stuff
    setTimeout(() => {
      /// Set the marker position
      this.marker.setPosition(latLng);

      /// Pan map to that position
      this.map.panTo(latLng);
    }, 300);
  };

  onInputChange = e => {
    this.setState({ searchText: e.target.value });
  };

  render() {
    return (
      <div id="app">
        <div id="app__interface">
          <div className="panel panel-default">
            <div className="panel-heading text-center">
              <span className="text-muted">
                Enter a place name below, drag the marker <em>or</em> click
                directly on the map
              </span>
            </div>
            <div className="panel-body">
              {/* Search Form - Ideally this should be moved out */}
              <form onSubmit={this.formSubmit}>
                <div className="input-group pull-left">
                  <input
                    type="text"
                    value={this.state.searchText}
                    className="form-control"
                    placeholder="Enter a town/city name"
                    onChange={this.onInputChange}
                  />
                  <span className="input-group-btn">
                    <button
                      type="submit"
                      className="btn btn-default"
                      onClick={this.locationSearch}
                    >
                      Search
                    </button>
                  </span>
                </div>
              </form>
            </div>
            <WeatherDetails
              location={this.state.location}
              weather={this.state.weather}
              icon={this.state.icon}
            />
          </div>
        </div>
        <div id="map" />
      </div>
    );
  }
}

export default WeatherApp;
