import React, { Component } from "react";
import "./App.scss";
import SearchBox from "./SearchBox";
import { MAP_API_KEY, WEATHER_API } from "./Constants";
import Carousel from "./Carousel";

class App extends Component {
  constructor(props) {
    super(props);
    this.state = {
      lat: 17.385, // hyderabad
      lang: 78.4867,
      cards: []
    };
    this.googleMapRef = React.createRef();
    this.googleMap = null;
    this.marker = null;
    this.polyline = null;
    this.infowindow = null;
  }

  componentDidMount() {
    const googleScript = document.createElement("script");
    googleScript.src = `https://maps.googleapis.com/maps/api/js?key=${MAP_API_KEY}&libraries=places`;
    window.document.body.appendChild(googleScript);

    googleScript.addEventListener("load", () => {
      this.googleMap = this.createGoogleMap(this.state.lat, this.state.lang);
      this.createMarkerAndPolyline();
      this.getWeatherData({ formatted_address: "Hyderabad" });
    });
  }

  createGoogleMap = () =>
    new window.google.maps.Map(this.googleMapRef.current, {
      zoom: 10,
      center: {
        lat: this.state.lat,
        lng: this.state.lang
      }
    });

  createMarkerAndPolyline = () => {
    this.marker = new window.google.maps.Marker({
      position: { lat: this.state.lat, lng: this.state.lang },
      map: this.googleMap
    });
    this.marker.addListener("click", () => {
      this.infowindow.open(this.googleMap, this.marker);
    });
    this.polyline = new window.google.maps.Polyline({
      strokeColor: "#000000",
      strokeOpacity: 1.0,
      strokeWeight: 3
    });
    this.polyline.setMap(this.googleMap);
    this.googleMap.setCenter(this.marker.getPosition());
  };

  getWeatherData = async city => {
    const url = `${WEATHER_API}&lat=${this.state.lat}&lon=${this.state.lang}`;
    const res = await fetch(url);
    const data = await res.json();
    const content = `<div className="content">
        <p>Temperature: ${data.main.temp}</p>
        <p>Humidity: ${data.main.humidity}</p>
        <p>Description: ${data.weather[0].description}</p>
      </div>`;
    this.infowindow = new window.google.maps.InfoWindow({
      content
    });
    const cards = [
      {
        city: city.formatted_address,
        temp: data.main.temp,
        humidity: data.main.humidity,
        desc: data.weather[0].description
      },
      ...this.state.cards
    ];
    this.setState({ cards });
  };

  onSelectedCity = city => {
    this.marker.setMap(null);
    const lat = city.geometry.location.lat();
    const lang = city.geometry.location.lng();
    this.setState({ lat, lang }, () => {
      this.createMarkerAndPolyline();
      this.getWeatherData(city);
    });
  };

  render() {
    return (
      <div className="body__style ">
        <div className="app__search ">
          <SearchBox onSelectedCity={this.onSelectedCity} />
        </div>
        <div
          id="google-map"
          ref={this.googleMapRef}
          className="google__map"
        />
        <Carousel children={this.state.cards} />
      </div>
    );
  }
}

export default App;
