import React, { Component } from "react";
import _ from 'lodash';
import ReactGoogleMapLoader from "react-google-maps-loader";
import ReactGooglePlacesSuggest from "react-google-places-suggest";
import { Input } from "semantic-ui-react";
import { API_KEY, MAP_API_KEY } from "./Constants";
import "./App.scss";

class SearchBox extends Component {
  constructor(props) {
    super(props);
    this.state = {
      search: "",
      value: ""
    };
  }

  handleInputChange(e) {
    this.setState({ search: e.target.value, value: e.target.value });
  }

  handleSelectSuggest(suggest) {
    console.log(suggest);
    this.setState({ search: "", value: suggest.formatted_address });
    this.props.onSelectedCity(suggest);
  }

  render() {
    const { search, value } = this.state;
    return (
      <ReactGoogleMapLoader
        params={{
          key: MAP_API_KEY,
          libraries: "places,geocode"
        }}  
        render={googleMaps =>
          googleMaps && (
            <div>
              <ReactGooglePlacesSuggest
                autocompletionRequest={{ input: search }}
                googleMaps={googleMaps}
                onSelectSuggest={this.handleSelectSuggest.bind(this)}
              >
              <span className="Search__Text">
                Search for cities,state and country 
              </span>
                <Input
                  placeholder="Search..."
                  className="app__search-search"
                  onChange={this.handleInputChange.bind(this)}
                  value={value}
                />
              </ReactGooglePlacesSuggest>
            </div>
          )
        }
      />
    );
  }
}


export default SearchBox;
