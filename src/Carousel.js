import React, { Component } from "react";
import { Swipeable } from "react-swipeable";
import { Grid } from "@material-ui/core";
import Card from '@material-ui/core/Card';
import CardContent from '@material-ui/core/CardContent';
import Typography from '@material-ui/core/Typography';
import IconButton from '@material-ui/core/IconButton';
import { blue } from '@material-ui/core/colors';

class Carousel extends Component {

  constructor(props) {
    super(props);
    this.state = {
      position: 0,
      direction: "next",
      sliding: false
    };
  }

  static getSlotWidth() {
    const cw = document.body.clientWidth;

    if (cw <= 599) return 80;
    if (cw <= 959) return 40;
    if (cw <= 1279) return 30;

    return 20;
  }

  getOrder = itemIndex => {
    const { position } = this.state;
    const { children } = this.props;
    const numItems = children.length || 1;

    return (numItems + 1 - position + itemIndex) % numItems;
  };

  nextSlide = () => {
    const { position } = this.state;
    const { children } = this.props;
    const numItems = children.length || 1;

    this.doSliding("next", position === numItems - 1 ? 0 : position + 1);
  };

  prevSlide = () => {
    const { position } = this.state;
    const { children } = this.props;
    const numItems = children.length;

    this.doSliding("prev", position === 0 ? numItems - 1 : position - 1);
  };

  doSliding = (direction, position) => {
    this.setState({
      sliding: true,
      direction,
      position
    });

    setTimeout(() => {
      this.setState({
        sliding: false
      });
    }, 50);
  };

  handleSwipe = isNext => {
    isNext ? this.nextSlide() : this.prevSlide();
  };

  render() {

    const { children, request } = this.props;
    const { sliding, direction } = this.state;

    const slotWidth = Carousel.getSlotWidth();

    return (
      <div>
        <Swipeable
          onSwipedLeft={() => this.handleSwipe(true)}
          onSwipedRight={() => this.handleSwipe()}
        >
          <div className="wrapper">
            <div
              style={{ display: "flex", margin: "20px 0" }}
              sliding={sliding}
              direction={direction}
              slotWidth={slotWidth}
            >
              {children.map((child, index) => (
                <div
                  className="card"
                  key={child.city}
                  order={this.getOrder(index)}
                >
                
                <Grid>
    <Card >
      <CardContent style={{backgroundColor: blue[700] ,color:'#fff'}} >
        <Typography  variant="title" component="h2" gutterBottom>
        {child.city}
        </Typography>
        <Typography  variant="h3" component="h2" gutterBottom >
          {child.temp}
        </Typography>
        <Typography variant="h5" component="h2" >
         {child.humidity}
        </Typography>
        <IconButton aria-label="Web Cloudy">
          </IconButton>
      </CardContent>
      <CardContent>
      <Typography variant="body2" component="p">
          {child.desc}
        </Typography>
        
      </CardContent>
    </Card>
    </Grid>
                 
                </div>
              ))}
            </div>
          </div>
        </Swipeable>

        <div className="buttons">
          <button onClick={this.prevSlide}>&#8592;Prev</button>
          <button onClick={this.nextSlide}>&#8594;Next</button>
        </div>
      </div>
    );
  }
}

export default Carousel;
