import React from "react";

const WeatherDetails = ({ location, weather, icon }) => {
  return (
    <div className="panel-heading weather">
      <p className="text-muted">
        <strong>{location}</strong>
      </p>
      <span className="text-muted">{weather}</span>
      <div className="weather__icon">
        <img src={icon} />
      </div>
    </div>
  );
};

export default WeatherDetails;
